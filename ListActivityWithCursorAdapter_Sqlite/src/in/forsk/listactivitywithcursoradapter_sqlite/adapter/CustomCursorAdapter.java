package in.forsk.listactivitywithcursoradapter_sqlite.adapter;

import com.androidquery.AQuery;

import in.forsk.listactivitywithcursoradapter_sqlite.R;
import in.forsk.listactivitywithcursoradapter_sqlite.DbAdapter.DBAdapter;
import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomCursorAdapter extends CursorAdapter {

	AQuery aq;

	public CustomCursorAdapter(Context context, Cursor cursor, int flags) {
		super(context, cursor, 0);

		aq = new AQuery(context);
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		ImageView profileIv = (ImageView) view.findViewById(R.id.profileIv);

		TextView nameTv = (TextView) view.findViewById(R.id.nameTv);
		TextView departmentTv = (TextView) view.findViewById(R.id.departmentTv);
		TextView reserch_areaTv = (TextView) view.findViewById(R.id.reserch_areaTv);

		AQuery temp_aq = aq.recycle(view);
		temp_aq.id(profileIv).image(cursor.getString(cursor.getColumnIndexOrThrow(DBAdapter.FACULTY_COLUMN_PHOTO)), true, true, 200, 0);

		nameTv.setText(cursor.getString(cursor.getColumnIndexOrThrow(DBAdapter.FACULTY_COLUMN_FIRSTNAME)) + " " + cursor.getString(cursor.getColumnIndexOrThrow(DBAdapter.FACULTY_COLUMN_LASTNAME)));
		departmentTv.setText(cursor.getString(cursor.getColumnIndexOrThrow(DBAdapter.FACULTY_COLUMN_DEP)));
		reserch_areaTv.setText(cursor.getString(cursor.getColumnIndexOrThrow(DBAdapter.FACULTY_COLUMN_EMAIL)));

	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		return LayoutInflater.from(context).inflate(R.layout.row_faculty_profile_list, parent, false);
	}

}
