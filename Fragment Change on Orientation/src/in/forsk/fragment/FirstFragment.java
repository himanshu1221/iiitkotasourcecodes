package in.forsk.fragment;

import in.forsk.MainActivity;
import in.forsk.R;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FirstFragment extends Fragment {

	public FirstFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		((MainActivity)getActivity()).notify("On Create of First Fragment");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_first, container, false);
		//To preserve the fragment view state
//		setRetainInstance(true);
		return rootView;
	}

}
