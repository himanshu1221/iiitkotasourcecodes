package in.forsk.listviewwithcustombaseadapter_viewholder;

import in.forsk.adapter.FacultyListAdapter;
import in.forsk.wrapper.FacultyWrapper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {
	private final static String TAG = MainActivity.class.getSimpleName();
	Context context;
	// List view reference
	ListView lv;

	// // /Data model
	// String[] data_array = new String[] { "Dr. Bharavi Mishra",
	// "Mr. Dinesh Khandelwal", "Mr. Mukesh K Jadon", "Dr. Poonam",
	// "Dr. PRAVEEN KUMAR", "Dr. Preety Singh", "Dr. Rajbir Kaur",
	// "Dr. Rajni Aron", "Dr. RATNADIP ADHIKARI", "Prof. Ravi Prakash Gorthi",
	// "Dr. Sakthi Balan Muthiah", "Mrs. Sonam Nahar", "Dr. Subrat Kumar Dash",
	// "Mr. Sunil Kumar", "Dr. VIBHOR KANT",
	// "Mr. Vikas Bajpai" };

	// Data model (Array List holding objects of FacultyWrapper )
	ArrayList<FacultyWrapper> mFacultyDataList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		context = this;

		// Creating reference
		lv = (ListView) findViewById(R.id.listView1);

		// Local File Parsing
		try {
			String json_string = getStringFromRaw(context, R.raw.faculty_profile_code);

			mFacultyDataList = pasreLocalFacultyFile(json_string);

			// setFacultyListAdapter(mFacultyDataList);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// creating array adapter as a bridge between tha data model and view
		// We are using Custom Array Adapter(FacultyListAdapter extent Array
		// Adapter)
		// We pass context and data model in our custom made adapter constructor
		// so we can use them there
		// we need context to init the layout inflater service , which inflate
		// the custom Views(Resource layout)
		FacultyListAdapter adapter = new FacultyListAdapter(context, R.layout.row_faculty_profile_list, mFacultyDataList);

		// Setting adapter to the list view, at this point list view use adapter
		// class methods to fill its view start the recycling process.
		lv.setAdapter(adapter);

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				// TODO Auto-generated method stub
				int itemPosition = position;

				// ListView Clicked item value
				// String itemValue = (String) lv.getItemAtPosition(position);

				// Show Alert
				Toast.makeText(getApplicationContext(), "Position :" + itemPosition, Toast.LENGTH_LONG).show();
			}
		});

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				Log.d(TAG, "getNativeHeapAllocatedSize - " + Debug.getNativeHeapAllocatedSize());
				Log.d(TAG, "getNativeHeapSize - " + Debug.getNativeHeapSize());
				Log.d(TAG, "getNativeHeapFreeSize - " + Debug.getNativeHeapFreeSize());
			}
		}, 2000);
	}

	private String getStringFromRaw(Context context, int resourceId) throws IOException {
		// Reading File from resource folder
		Resources r = context.getResources();
		InputStream is = r.openRawResource(resourceId);
		String statesText = convertStreamToString(is);
		is.close();

		Log.d(TAG, statesText);

		return statesText;
	}

	private String convertStreamToString(InputStream is) throws IOException {
		// Converting input stream into string
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int i = is.read();
		while (i != -1) {
			baos.write(i);
			i = is.read();
		}
		return baos.toString();
	}

	public ArrayList<FacultyWrapper> pasreLocalFacultyFile(String json_string) {

		ArrayList<FacultyWrapper> mFacultyDataList = new ArrayList<FacultyWrapper>();
		try {
			// Converting multipal json data (String) into Json array
			JSONArray facultyArray = new JSONArray(json_string);
			Log.d(TAG, facultyArray.toString());
			// Iterating json array into json objects
			for (int i = 0; facultyArray.length() > i; i++) {

				// Extracting json object from particular index of array
				JSONObject facultyJsonObject = facultyArray.getJSONObject(i);

				// Design patterns
				FacultyWrapper facultyObject = new FacultyWrapper(facultyJsonObject);

				printObject(facultyObject);

				mFacultyDataList.add(facultyObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mFacultyDataList;
	}

	public void printObject(FacultyWrapper obj) {
		// Operator Overloading
		Log.d(TAG, "First Name : " + obj.getFirst_name());
		Log.d(TAG, "Last Name : " + obj.getLast_name());
		Log.d(TAG, "Photo : " + obj.getPhoto());
		Log.d(TAG, "Department : " + obj.getDepartment());
		Log.d(TAG, "reserch_area : " + obj.getReserch_area());
		Log.d(TAG, "Phone : " + obj.getPhone());
		Log.d(TAG, "Email : " + obj.getEmail());

		for (String s : obj.getInterest_areas()) {
			Log.d(TAG, "Interest Area : " + s);
		}
	}
}
